package handlers

//noinspection GoUnresolvedReference
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	//v1alpha1 "github.com/containous/traefik/v2/pkg/provider/kubernetes/crd/traefik/v1alpha1"
	"github.com/julienschmidt/httprouter"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	v1beta1 "k8s.io/api/extensions/v1beta1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	intstr "k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type CreateEdgeyModel struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
	Owner     string `json:"owner"`
	Token     string `json:"token"`
	Image     string `json:"image"`
}

type EdgeModel struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
	Owner     string `json:"owner"`
	Status    string `json:status`
}

type EdgeListModel struct {
	List []EdgeModel `json:"list"`
}

// CreateEdgeHandle a function invocation for creating a device connector
func CreateEdgeHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model CreateEdgeyModel
		json.Unmarshal(input, &model)

		config, err := rest.InClusterConfig()

		if err != nil {
			fmt.Printf("couldn't get cluster config: %s\n", err)
			os.Exit(-1)
		}

		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			fmt.Printf("couldn't get clientset: %s\n", err)
			os.Exit(-1)
		}

		deployErr := createIoTBrokerDeployment(model.Name, model.Owner, model.Namespace, model.Token, model.Image, clientset)
		if deployErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker server deployment %s", deployErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		serviceErr := createClusterIP(model.Name, model.Owner, clientset)
		if serviceErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker service %s", serviceErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(""))
	}
}

// GetFileHandle a function invocation
func GetEdgeGateway() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(""))
	}
}

func createClusterIP(name, ns string, clientset *kubernetes.Clientset) error {
	service := clientset.CoreV1().Services(ns)
	serviceSpec := &corev1.Service{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Service",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: ns,
		},
		Spec: corev1.ServiceSpec{
			Type: corev1.ServiceTypeClusterIP,
			Selector: map[string]string{
				"app": "edge_broker",
			},
			Ports: []corev1.ServicePort{
				{
					Name:     "broker",
					Protocol: corev1.ProtocolTCP,
					Port:     8000,
					TargetPort: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: 8000,
					},
				},
			},
		},
	}
	_, err := service.Create(serviceSpec)

	if err != nil {
		wrappedErr := fmt.Errorf("failed create Service: %s", err.Error())
		log.Println(wrappedErr)
		return wrappedErr

	}

	log.Printf("Service created: %s.%s\n", name, ns)
	return nil
}

// ListEdgeHandle a function invocation for query on device connectors
func ListEdgeHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model EdgeModel
		json.Unmarshal(input, &model)

		config, err := rest.InClusterConfig()

		if err != nil {
			fmt.Printf("couldn't get cluster config: %s\n", err)
			os.Exit(-1)
		}

		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			fmt.Printf("couldn't get clientset: %s\n", err)
			os.Exit(-1)
		}

		listOpts := metav1.ListOptions{
			LabelSelector: "component=edge_broker",
		}

		podList, err := clientset.CoreV1().Pods(model.Namespace).List(listOpts)
		if err != nil {
			fmt.Printf("couldn't list pods in %s: %s\n", model.Namespace, err.Error())
			os.Exit(-1)
		}

		var deviceList EdgeListModel
		for _, item := range podList.Items {
			newDevice := EdgeModel{
				Name:      item.Labels["device_name"],
				Namespace: item.Namespace,
				Status:    podStatus(item.Status.Phase),
				Owner:     item.Labels["owner"],
			}

			deviceList.List = append(deviceList.List, newDevice)
			fmt.Printf("\nThe function is: %s - status: %v - %v", item.Labels["device_name"], item.Status.ContainerStatuses, item.Status.Phase)
		}

	}
}

func createIngress(name, ns, path string, clientset *kubernetes.Clientset) error {
	ingressClient := clientset.ExtensionsV1beta1().Ingresses(ns)
	ingress := &v1beta1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1beta1.IngressSpec{
			Rules: []v1beta1.IngressRule{
				{
					IngressRuleValue: v1beta1.IngressRuleValue{
						HTTP: &v1beta1.HTTPIngressRuleValue{
							Paths: []v1beta1.HTTPIngressPath{
								{
									Path: path,
									Backend: v1beta1.IngressBackend{
										ServiceName: "service1",
										ServicePort: intstr.FromInt(80),
									},
								},
							},
						},
					},
				},
			},
		},
	}
	_, err := ingressClient.Create(ingress)
	if err != nil {
		return err
	}

	return nil
}

func createIoTBrokerDeployment(edgeDeviceName, owner, namespace, token, image string, clientset *kubernetes.Clientset) error {

	deploymentsClient := clientset.AppsV1().Deployments(namespace)
	labels := make(map[string]string)
	labels["app"] = edgeDeviceName
	labels["app"] = "edge_broker"
	labels["owner"] = owner
	labels["app.kubernetes.io/name"] = edgeDeviceName
	labels["app.kubernetes.io/app"] = "edge_broker"
	labels["app.kubernetes.io/part-of"] = namespace
	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      edgeDeviceName,
			Namespace: namespace,
			Labels:    labels,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: int32Ptr(1),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"edge_name": edgeDeviceName,
					"app":       "edge_broker",
				},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: namespace,
					Annotations: map[string]string{
						"prometheus.io.scrape": "false",
					},
					Labels: map[string]string{
						"edge_name": edgeDeviceName,
						"app":       "edge_broker",
					},
				},
				Spec: corev1.PodSpec{
					Volumes: []corev1.Volume{
						{
							Name: "auth",
							VolumeSource: corev1.VolumeSource{
								Secret: &corev1.SecretVolumeSource{
									SecretName: "basic-auth",
								},
							},
						},
					},
					Containers: []corev1.Container{
						{
							Name:            edgeDeviceName,
							Image:           image,
							ImagePullPolicy: "IfNotPresent",
							Resources: corev1.ResourceRequirements{
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("100m"),
									"memory": resource.MustParse("100Mi"),
								},
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("100m"),
									"memory": resource.MustParse("100Mi"),
								},
							},
							Env: []corev1.EnvVar{
								{
									Name:  "TOKEN",
									Value: token,
								},
							},
							Ports: []corev1.ContainerPort{
								{ContainerPort: 8080},
							},
						},
					},
				},
			},
		},
	}

	// Create Deployment
	fmt.Println("Creating deployment...")
	result, err := deploymentsClient.Create(deployment)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Created deployment %q.\n", result.GetObjectMeta().GetName())

	return nil
}

func createSecret(name, namespace string, data map[string][]byte) {

}
