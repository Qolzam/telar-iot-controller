package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type DeviceMode struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type CreateDeviceModel struct {
	DeviceMode
	Gateway EdgeGateway `json:"gateway"`
}

type DeviceStatusModel struct {
	DeviceMode
	Gateway EdgeGateway `json:"gateway"`
}

type DeviceListModel struct {
	List []DeviceStatusModel `json:"gateway"`
}

// CreateDeviceHandle a function invocation for creating a device connector
func CreateDeviceHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model CreateDeviceModel
		json.Unmarshal(input, &model)

		newReq, _ := http.NewRequest(http.MethodPost, model.Gateway.GatewayURL+"/function/edge-controller", r.Body)
		newReq.Header.Set("Content-Type", "application/octet-stream")

		_, err := http.DefaultClient.Do(newReq)

		if err != nil {
			log.Printf("Create device request error: %s\n", err)

		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(""))
	}
}

// GetDviceStatusListHandle a function invocation to get list of devices status
func GetDviceStatusListHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model EdgeGateway
		json.Unmarshal(input, &model)

		newReq, _ := http.NewRequest(http.MethodGet, model.GatewayURL+"/function/edge-controller", r.Body)
		res, err := http.DefaultClient.Do(newReq)

		if err != nil {
			log.Printf("Create device request error: %s\n", err)

		}
		var resInput []byte
		if res.Body != nil {
			defer res.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			resInput = body
		}

		w.WriteHeader(http.StatusOK)
		w.Write(resInput)
	}
}
