module gitlab.com/Qolzam/telar-iot-controller

go 1.14

require (
	cloud.google.com/go v0.51.0 // indirect
	github.com/Azure/go-autorest/autorest v0.9.6 // indirect
	github.com/Qolzam/openfaas-cloud v0.0.0-20200318095548-f1c27b4430c8
	github.com/alexellis/hmac v0.0.0-20180624211220-5c52ab81c0de // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/googleapis/gnostic v0.4.0 // indirect
	github.com/gophercloud/gophercloud v0.1.0 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/openfaas/faas v0.0.0-20191125105239-365f459b3f3a // indirect
	github.com/openfaas/faas-netes v0.0.0-20200508140708-27ed33a81af1
	github.com/openfaas/faas-provider v0.15.1
	github.com/pkg/errors v0.8.1 // indirect
	github.com/prometheus/client_golang v0.9.2 // indirect
	go.uber.org/goleak v1.0.0 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	k8s.io/api v0.18.2
	k8s.io/apimachinery v0.18.2
	k8s.io/client-go v0.17.4
	k8s.io/code-generator v0.17.4 // indirect
	k8s.io/gengo v0.0.0-20200413195148-3a45101e95ac // indirect
	k8s.io/utils v0.0.0-20200414100711-2df71ebbae66 // indirect
)
